fn main() {
    // Expose pyo3's cfg values, for conditional compilation.
    pyo3_build_config::use_pyo3_cfgs();
}
