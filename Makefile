.PHONY: build

build:
	pip install .

test: build
	pytest
